<?php

use yii\db\Migration;

class m220211_185143_update_user extends Migration
{

    public function safeUp()
    {

      $this->addColumn('user', 'secondname', $this->string()->after('username'));
      $this->addColumn('user', 'name', $this->string()->after('username'));
      $this->addColumn('user', 'about', $this->string(500));
      $this->addColumn('user', 'foto', $this->string());
      $this->addColumn('user', 'date', $this->date('Y-m-d'));

    }
}
