<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$this->title = 'Пользователи';

?>
<div class="user-index">
  <div class="row">

    <?php foreach ($users as $user): ?>
        <div class="col-lg-4 col-md-6 col-sm-12">
          <p>
              <?php
              $path = $user['foto'];
              echo "<h4>".$user['username']."</h4>";
              echo "<p><img src='$path' alt='' width='50%'></p>";
              echo $user['firstname'].' '.$user['name'].' '.$user['secondname']."<br/>";
              echo "<p>".Html::a('Профиль', ['view', 'id' => $user['id']], ['class' => 'btn btn-primary'])."</p>";
            ?>
          </p>
        </div>
    <?php endforeach; ?>

    <div class="col-lg-12 col-md-12 col-sm-12">
      <?= yii\widgets\LinkPager::widget(['pagination' => $pages]); ?>
    </div>

</div>
</div>
