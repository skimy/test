<?php

use yii\helpers\Html;

$this->title = 'Изменение данных профиля: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Все пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="boxfttb-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
