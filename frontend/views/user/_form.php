<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use yii\helpers\ArrayHelper;
use common\models\User;

?>

<div class="user-form">
  <div class="row">
    <div class="col-lg-3 col-md-4 col-sm-12">
      <p>
        <?php
          $path = $model->foto;
          echo "<img src='$path' alt='' width='100%'>";
        ?>
      </p>
      <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
      <?= $form->field($model, 'file')->fileInput() ?>
    </div>

    <div class="col-lg-9 col-md-8 col-sm-12">

    <?= $form->field($model, 'firstname')->textInput(array('placeholder' => 'Введите фамилию')); ?>
    <?= $form->field($model, 'name')->textInput(array('placeholder' => 'Введите имя')); ?>
    <?= $form->field($model, 'secondname')->textInput(array('placeholder' => 'Введите отчество')); ?>
    <?= $form->field($model, 'about')->textarea(['rows'=>3])->label('Напишите о себе'); ?>

    <?= $form->field($model,'date')->widget(DatePicker::class, [
        'language' => 'ru',
        'dateFormat' => 'yyyy.MM.dd',
        'options' => [
            'class'=> 'form-control',
            'autocomplete'=>'off'
        ],
        'clientOptions' => [
            'changeMonth' => true,
            'changeYear' => true,
            'yearRange' => '1940:2050',
        ]])->label("Выберите дату рождения") ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
  </div>
</div>

</div>
