<?php
namespace frontend\controllers;

use Yii;
use common\models\User;
use common\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\data\Pagination;

class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

     public function actionIndex()
    {
        $query =  User::find()->andFilterWhere(['not like', 'username', "admin"]);
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 9]);
        $users = $query->offset($pages->offset)->limit($pages->limit)->all();
        return $this->render('index', compact('users', 'pages'));
    }


    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionUpdate($id)
    {
      if (Yii::$app->user->id != $id) {return $this->goHome();}
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
          $fotoName = $model->id;
          $model->file=UploadedFile::getInstance($model,'file');
          $ext = substr(strrchr($model->file,'.'),1);
          if($ext != null) {
            $model->file->saveAs('uploads/' . $fotoName . '.' . $model->file->extension);
            $model->foto = '/uploads/' . $fotoName . '.' . $model->file->extension;
            $model->save();
          }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
       }
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
