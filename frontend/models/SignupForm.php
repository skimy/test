<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;

class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $password_validate;
    public $rememberMe = true;

    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['password', 'required'],
            ['password', 'string', 'min' => Yii::$app->params['user.passwordMinLength']],
            ['password_validate', 'string', 'min' => Yii::$app->params['user.passwordMinLength']],

        ];
    }


    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        if ($this->password != $this->password_validate) {
          Yii::$app->session->setFlash('warning', 'Пароли не совпадают');
            return null;
        }

        $user = new User();
        $user->username = $this->username;
        $user->status = 10;
        $user->setPassword($this->password);
        $user->generateAuthKey();

        return $user->save();
    }

    public function attributeLabels()
       {
           return [
               'username' => 'Логин',
               'password' => 'Пароль',
               'password_validate' => 'Повторите пароль',
           ];
       }

}
