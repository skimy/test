<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$this->title = 'Пользователи';

?>
<div class="user-index">

  <?=
    Html::a('Добавить пользователя', ['create'], ['class' => 'btn btn-success']);
  ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn',
            'contentOptions' => ['style'=> 'width: 20px']],

            [
                'attribute'=>'username',
                'contentOptions' => ['style'=> 'width: 30px'],
            ],
            [
                'attribute'=>'firstname',
                'contentOptions' => ['style'=> 'width: 30px'],
            ],
            [
                'attribute'=>'name',
                'contentOptions' => ['style'=> 'width: 30px'],
            ],
            [
                'attribute'=>'secondname',
                'contentOptions' => ['style'=> 'width: 30px'],
            ],
            [
                'attribute'=>'date',
                'contentOptions' => ['style'=> 'width: 110px'],
            ],
            [
                'attribute'=>'about',
            ],
            [
                'label' => 'Фото',
                'format' => 'raw',
                'contentOptions' => ['style'=> 'width: 60px'],
                'value' => function($data){
                    return Html::img($data->foto,[
                        'style' => 'width:50px;']);
                },
            ],
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'contentOptions' => ['style' => 'width: 20px'],
                ],
          ],
    ]);
    ?>
</div>
