<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\url;


$this->title = $model->username;
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">

<div class="col-lg-3 col-md-4 col-sm-12">
  <p>
    <?php
      $path = $model->foto;
      echo "<img src='$path' alt='' width='100%'>";
    ?>
  </p>
</div>

<div class="col-lg-9 col-md-8 col-sm-12">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'firstname',
            'name',
            'secondname',
            'date',
            'about',
        ],
      ]);
    ?>
    <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']); ?>
    <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Ты уверен, что хочешь удалить юзера?!',
                    'method' => 'post',
                ],
            ]) ?>
  </div>

</div>
