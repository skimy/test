<?php
namespace backend\controllers;

use Yii;
use common\models\User;
use common\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use frontend\models\SignupForm;

class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

     public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
          $fotoName = $model->id;
          $model->file=UploadedFile::getInstance($model,'file');
          $ext = substr(strrchr($model->file,'.'),1);
          if($ext != null) {
            $model->file->saveAs(Yii::getAlias('@frontend').'/web/uploads/' . $fotoName . '.' . $model->file->extension);
            $model->foto = '/uploads/' . $fotoName . '.' . $model->file->extension;
            $model->save();
          }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
       }
    }

    public function actionCreate()
      {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', 'Пользователь зарегистрирован');
            $user = User::find()->where(['username' => $model->username])->one();
            return $this->redirect(['update', 'id' => $user->id]);
        }
        return $this->render('signup', ['model' => $model,]);
      }

    public function actionDelete($id)
    {
        $model=$this->findModel($id);
        if($model->foto != null) {unlink(Yii::getAlias('@frontend/web/'.$model->foto));}
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
