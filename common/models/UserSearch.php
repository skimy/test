<?php
namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;

class UserSearch extends User
{

    public function rules()
    {
        return [
            [['username', 'name',], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = User::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'username', $this->username])
        ->andFilterWhere(['like', 'name', $this->name])
        ->andFilterWhere(['not like', 'username', "admin"])
        ;
        $dataProvider->pagination->pageSize = 20;
        return $dataProvider;
    }
}
