<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
      'formatter' => [
          'class' => 'yii\i18n\Formatter',
          'nullDisplay' => '',
        ],
      'cache' => [
          'class' => 'yii\caching\FileCache',
        ],
    ],
];
